#include "linked_list.h"

int main() {
    //test initializer
    linked_list myList(0);
    myList.print();

    //test append and array initializer
    int a[3]={1, 2, 3};
    myList.append(a, 3);
    myList.print();

    //test print middle (even number of nodes)
    myList.print_middle();

    //test insert
    myList.insert(4, 4);
    myList.print();

    //test print middle (odd number of nodes)
    myList.print_middle();

    //test remove
    myList.remove(0);
    myList.print();

    return 0;
}