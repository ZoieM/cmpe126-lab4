#include "node.h"
#include <iostream> //to output error message

// Take in value and create a node
node::node(int input)
{
    data = input;
    next = nullptr; //Not sure if I need to put the node in the right spot.
}
// Takes in an array of values and creates the appropriate nodes
node::node(int values[], int length) {
    if (length <= 0)//Check for errors
    {
        //error!!
        std::cout << "ERROR: Invalid length!!! Ignore rest of code." << std::endl;
        //I have to create something so:
        data = 0;
        next = nullptr;
    }
    else if (length == 1) {
        data = values[0];
        next = nullptr;
    }
    else //if length >= 2
    {
        //1. Create nodes last through 2nd and save their addresses.
        node *tmp[length-1];//array of node pointers to store addresses
        for(int i=length-1; i>0; i--)
        {
            tmp[i-1] = new node(values[i]);//does not require & b/c "new" returns the address
        }
        //2. Define the first node
        data = values[0];
        next = tmp[0];
        //3. Use the addresses we saved to update the "next" of remaining nodes (the first and last nodes are done)
        for(int j=0; j<=length-3; j++)
        {
            (*tmp[j]).next = tmp[j+1];
        }
    }
}
// Add a value to the end as a node
void node::append(int input)
{
    //find the last node and change its "next" to the address of the new node
    node *runner = this;
    while(runner->next != nullptr)
    {
        runner = runner->next;
    }
    runner->next = new node(input);
}

// Add an array of values to the end as separate nodes
void node::append(int inputs[], int length)
{
    //find the last node and change its "next" to the address of the first new node
    node *runner = this;
    while(runner->next != nullptr)
    {
        runner = runner->next;
    }
    runner->next = new node(inputs, length);
}

// Insert a new node after the given location
node* node::insert(int location, int value)
{
    if(location > 0)
    {
        //go to the node in the position "location-1"
        node *runner = this, *newNode, *tmp;
        for(int i=0; i<location-1; i++)
        {
            if(runner->next != nullptr)
            {
                runner = runner->next;
            }
            else//make sure runner doesn't go past the end of the linked list
            {
                break; //leave for loop
            }
        }
        newNode = new node(value);
        tmp = runner->next;
        (*runner).next = newNode;//make "location-1" point to new node
        (*newNode).next = tmp;//make new node point to node previously in the "location" position
    }
    else//location == 0
    {
        node *newNode, *tmp = this->next;
        newNode = new node(value);
        this->next = newNode;//make "location-1" point to new node
        (*newNode).next = tmp;//make new node point to node previously in the "location" position
    }
    // Must return head pointer location. But can never replace head if insert is always after.
    return this;
}

// Remove a node and link the next node to the previous node
node* node::remove(int location)
{
    if(location > 0)
    {
        //go to the node in the position "location-1"
        node *runner = this, *tmp;
        for(int i=0; i<location-1; i++)
        {
            if(runner->next->next != nullptr)
            {
                runner = runner->next;
            }
            else//location is the last node (or we will treat it like it is)
            {
                break; //leave for loop to make sure we don't go off the end of the linked list
            }
        }
        tmp = runner->next->next;
        runner->next = tmp;
        //(We don't have a destructor for the node class)

        // Must return head pointer location
        return this;
    }
    else//location == 0
    {
        //(We don't have a destructor for the node class)

        // Must return head pointer location
        return this->next;
    }
}

// Print all nodes
void node::print()
{
    node *runner = this;
    if(runner->next == nullptr)//if there is only one node, just print it
    {
        std::cout << runner->data << std::endl;
    }
    else//if there is more than one node
    {
        while(runner->next != nullptr)//print and increment until the last node
        {
            std::cout << runner->data << " " << std::flush;
            runner = runner->next;
        }
        //print the last node.
        std::cout << runner->data << std::endl;
    }

}

//Print the middle node
void node::print_middle()
{
    // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
    //       and two nodes per step. When the faster one reaches the end, the slow one should be
    //       pointing to the middle
    node *runner1 = this, *runner2 = this;
    while(runner2->next!= nullptr)
    {
        if((runner2->next->next)==nullptr)//If even # of nodes, print the lower of the middle two
        {
            runner2 = runner2 -> next;
        }
        else//If odd # of nodes, print middle
        {
            runner1 = runner1->next;
            runner2 = runner2->next->next;
        }
    }
    std::cout << runner1->data << std::endl;
}

// Get the value of a given node
int node::get_value(int location)
{
    node *runner = this;
    //assume we start at head (0) and need to output data of the (location)th node
    for(int i=0; i<location; i++)
    {
        runner = runner->next;
    }
    return runner->data;
}

// Overwrite the value of a given node
void node::set_data(int location, int value)
{
    node *runner = this;
    //assume we start at head (0) and need to output data of the (location)th node
    for(int i=0; i<location; i++)
    {
        runner = runner->next;
    }
    runner->data = value;
}
